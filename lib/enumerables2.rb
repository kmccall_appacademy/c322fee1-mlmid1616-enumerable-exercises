require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |string| string.include?(substring) end
end


# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.delete!(" ")
  repeating_letters = []
  string.chars.each_with_index do |letter1,idx1|
    string.chars.each_with_index do |letter2,idx2|
      next if idx1 == idx2
      repeating_letters << letter1 if letter2 == letter1
    end
  end
  repeating_letters.sort.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete("!.,:?'")
  list = string.split.sort_by do |word| word.length end
  new_list = []
  new_list[0] = list[-1]
  new_list[1] = list[-2]
  new_list
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ("a".."z").to_a
  string.chars.each do |letter| alphabet.delete(letter) end
  alphabet
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select do |year| not_repeat_year?(year) end
end

def not_repeat_year?(year)
  year.to_s.chars.each_with_index do |digit1,idx1|
    year.to_s.chars.each_with_index do |digit2,idx2|
      next if idx1 == idx2
      return false if digit1 == digit2
    end
  end
  true
end

# def not_repeat_year?(year)
#   i = 0
#   while i < year.to_s.length
#     c = i + 1
#     while c < year.to_s.length
#       array = year.to_s.chars
#       return false if array[i] == array[c]
#       c += 1
#     end
#   i+= 1
#  end
# true
# end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs_to_delete = []
  songs.each_with_index do |song,idx|
    next if idx == songs.length - 1
    songs_to_delete << song if song == songs[idx+1]
  end
    songs_to_delete.each do |song_to_be_deleted| songs.delete(song_to_be_deleted) end
    songs.uniq
end

# def no_repeats?(song_name, songs)
#   good_weeks = songs.count do |listed_song| song_name == listed_song end
#   good_weeks > 1 ? false : true
# end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.delete!("!?.:',")
  the_c_gang = string.split.select do |word| word.include?("c") end
  sorted_c_gang = the_c_gang.sort_by do |word| c_distance(word) end
  sorted_c_gang_distance = c_distance(sorted_c_gang.first)
  string.split.each do |word| return word if c_distance(word) == sorted_c_gang_distance end
end

def c_distance(word)
  return word.length unless word.include?("c")
  word.length - word.rindex("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  nested_arrays = []
  idx_keeper = 0
  arr.each_with_index do |num,idx|
    if num == arr[idx+1] && num != arr[idx-1]
      nested_arrays << [idx,0]
    elsif num == arr[idx-1] && num != arr[idx+1]
      nested_arrays[idx_keeper][1] = idx
      idx_keeper += 1
    end
  end
    nested_arrays
end
